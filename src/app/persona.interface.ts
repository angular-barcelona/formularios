import { Profesion } from './profesion.interface';

export interface Persona {
  nombre: string;
  apellidos: string;
  profesion: number | string;
}
