import { Component } from '@angular/core';
import { Persona } from './persona.interface';
import { Profesion } from './profesion.interface';

@Component({
  selector: 'form-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public profesiones: Profesion[] = [
    {
      id: 1,
      nombre: 'Programador TS'
    },
    {
      id: 2,
      nombre: 'Estrella del pop'
    }
  ];

  public persona: Persona = {
    nombre: '',
    apellidos: '',
    profesion: ''
  };
  public acepta = false;

  public enviarDatos(formulario) {
    formulario.resetForm();
    this.guardaPersonaBD(this.persona);
  }

  private guardaPersonaBD(persona: Persona) {

  }
}
